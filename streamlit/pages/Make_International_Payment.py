import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from io import StringIO
from openai import OpenAI
import os
from dotenv import load_dotenv
load_dotenv()

client = OpenAI(
    # This is the default and can be omitted
    api_key=os.environ.get("OPENAI_API_KEY"),
)
model = "gpt-4o"
def get_openai_response():
    chat_completion = client.chat.completions.create(
    messages=st.session_state.messages,
    model=model,
    )
    return chat_completion.choices[0].message.content.strip()

st.set_page_config(page_title="Make an International Payment", layout="wide")
st.sidebar.header("Make an International Payment")

st.markdown("""
    <style>
        header {
            display: none!important;
        }
        .header-container {
            background: #5a287d;
            position: fixed;
            top: 0;
            left: 0;
            height: 70px;
            width: 100%;
        }
        .header-container img {
            height: 50px;
            margin-left: 70px;
            margin-top: 9px;
        }
        .block-container button {
            background-color: #5e10b1!important;
            color: #fff!important;
            border: none!important;
            border-radius: 25px!important;
            padding: 10px 30px!important;
        }
            
        .block-container [data-testid="column"] {
        }
    </style>
    <div class="header-container">
        <img src="https://www.bankline.natwest.com/CWSLogon/resources/images/logo-nwb.svg" class="brand-logo" alt="NatWest Logo">
    </div>
            """, unsafe_allow_html=True)

st.title("Make an International Payment")

if 'messages' not in st.session_state:
    st.session_state.messages = []
    st.session_state.input_text = ""

col1, col2, col3 = st.columns(3)

with col1:
    st.subheader('From')
    st.selectbox(
        "Account",
        ("Head Office Suppliers - 19746347 08-03-02", "Loans Account - 83728473 08-03-02"),
        index=None,
        placeholder="Select an account.")
    st.text_input("Your reference (optional)")

with col2:
    st.subheader('To')
    country = st.selectbox(
        "Country where the account is based",
        ("United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela"),
        index=None,
        placeholder="Select a destination country.",
    )
    if country and len(st.session_state.messages) == 0:
        st.session_state.messages = [
            {"role": "assistant", "content": "Hi John, I'm SKIP, I can see you want to make an international payment. I'm here to help you with this.", "hidden": False},
            {"role": "assistant", "content": "I can see you want to make a payment from your \"Head Office Suppliers\" account to " + country + ". Is this correct?", "hidden": False},
            {"role": "system", "content": "For a payment to " + country + " you will only ask for the 'Purpose of Payment' code. If they are unsure of the correct one help them.", "hidden": True},
            {"role": "system", "content": "Valid purpose of payment codes for " + country + " are SAL, SAA, FAM, N/A", "hidden": True},
        ]

    if country:
        currency = st.selectbox(
            "Currency being sent",
            ("GBP", "USD", "EUR"),
            index=None,
            placeholder="Select a currency",
        )
        name = st.text_input(
            "Name of business or person"
        )
        iban = st.text_input(
            "IBAN",
        )
        bic = st.text_input(
            "Bank Identifier Code (BIC) (Optional)",
        )
        if st.button('Use this payee'):
            st.session_state.messages.append({"role": "user", "content": "I am trying to make a payment to " + name + " in " + country + " what do I put in the reference field?", "hidden": True})
            response = get_openai_response()
            st.session_state.messages.append({"role": "assistant", "content": response, "hidden": False})

with col3:
    def submit():
        st.session_state.input_text = st.session_state.input_widget
        st.session_state.input_widget = ""

    # Sending Messages
    if st.session_state.input_text != "":
        st.session_state.messages.append({"role": "user", "content": st.session_state.input_text, "hidden": False})
        st.session_state.input_text = ""
        response = get_openai_response()
        st.session_state.messages.append({"role": "assistant", "content": response, "hidden": False})

    # Display chat
    if len(st.session_state.messages) > 0:
        messages = st.container(height=550)

        for message in st.session_state.messages:
            if message["hidden"]:
                continue
            if message["role"] == "user":
                messages.chat_message("user").write(message['content'])
                # st.write(f"You: {message['content']}")
            else:
                messages.chat_message("assistant").write(message['content'])
                # st.write(f"Assistant: {message['content']}")

        # Chat input
        st.text_input("You: ", key="input_widget", on_change=submit)
